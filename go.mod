module modernc.org/y

go 1.15

require (
	modernc.org/mathutil v1.6.0
	modernc.org/parser v1.1.0
	modernc.org/sortutil v1.2.0
	modernc.org/strutil v1.2.0
)
